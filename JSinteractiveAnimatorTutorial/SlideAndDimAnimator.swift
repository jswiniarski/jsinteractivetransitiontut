//
//  SlideAndDimAnimator.swift
//  Ruwer
//
//  Created by Jerzyk on 23/05/2017.
//  Copyright © 2017 inFullMobile. All rights reserved.
//

import Foundation
import UIKit

class SlideAndDimAnimator: NSObject {
    final var duration: TimeInterval
    final var isHiding: Bool
    final var blurAmount: Float
    final var interactive: Bool

    final fileprivate var blurView: UIView?

    init(duration: TimeInterval = 0.5, isHiding: Bool, blurAmount: Float = 1.0, interactive: Bool) {
        self.isHiding = isHiding
        self.duration = duration
        self.blurAmount = blurAmount
        self.interactive = interactive
        super.init()
    }

    func blurView(forView view: UIView) -> UIView {
        if UIAccessibilityIsReduceTransparencyEnabled() {
            //blur effect disabled - add white background
            let aView = UIView()
            aView.frame = view.bounds
            aView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            aView.backgroundColor = UIColor.white
            return aView
        } else {
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            return blurEffectView
        }
    }
}

extension SlideAndDimAnimator : UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
            else {
                return
        }
        let containerView = transitionContext.containerView

        if self.isHiding {
            fromVC.view.frame.origin.y = toVC.view.frame.minY
            UIView.animate(withDuration: duration,
                           animations: {
                            fromVC.view.frame.origin.y = toVC.view.frame.maxY
                            self.blurView?.alpha = 0.0
            },
                           completion: { _ in
                            transitionContext.completeTransition(true)
            })
        } else {
            containerView.addSubview(toVC.view)
            self.blurView = self.blurView(forView: fromVC.view)
            self.blurView?.alpha = 0.0
            if let blurView = self.blurView {
                containerView.insertSubview(blurView, belowSubview: toVC.view)
            }

            toVC.view.frame.origin.y = fromVC.view.frame.maxY

            UIView.animate(withDuration: duration,
                           animations: {
                            self.blurView?.alpha = CGFloat(self.blurAmount)
                            toVC.view.frame.origin.y = fromVC.view.frame.minY
            },
                           completion: { _ in
                            transitionContext.completeTransition(true)
            })
        }

    }
}

