//
//  SlideAndDimInteractionController.swift
//  Ruwer
//
//  Created by Jerzyk on 24/05/2017.
//  Copyright © 2017 inFullMobile. All rights reserved.
//

import Foundation
import UIKit

class SlideAndDimInteractionController: UIPercentDrivenInteractiveTransition {
    var interactionInProgress = false
    private var shouldCompleteTransition = false
    private weak var viewController: UIViewController!
    
    func wireToViewController(_ viewController: UIViewController) {
        self.viewController = viewController
        prepareGestureRecognizerInView(view: viewController.view)
    }
    
    private func prepareGestureRecognizerInView(view: UIView) {
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(handleGesture(gestureRecognizer:)))
        view.addGestureRecognizer(gesture)
    }
    
    func handleGesture(gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        
        let translation = gestureRecognizer.translation(in: gestureRecognizer.view!.superview!)
        print("translation: \(translation)")
        var progress = (translation.y / UIScreen.main.bounds.height)
        progress = CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))
        print("progress: \(progress)")
        
        switch gestureRecognizer.state {
            
        case .began:
            print("began")
            interactionInProgress = true
            viewController.dismiss(animated: true, completion: nil)
            
        case .changed:
            print("changed")
            shouldCompleteTransition = progress > 0.6
            update(progress)
            
        case .cancelled:
            print("cancelled")
            interactionInProgress = false
            cancel()
            
        case .ended:
            print("ended")
            interactionInProgress = false
            
            if !shouldCompleteTransition {
                print("cancelling")
                cancel()
            } else {
                print("finishing")
                finish()
            }
            
        default:
            print("Unsupported")
        }
    }
}
