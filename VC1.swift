//
//  VC1.swift
//  JSinteractiveAnimatorTutorial
//
//  Created by Jerzyk on 24/05/2017.
//  Copyright © 2017 Jerzyk. All rights reserved.
//

import Foundation
import UIKit

class VC1: UIViewController {
    
    fileprivate final let animator = SlideAndDimAnimator(duration: 1.5, isHiding: false, interactive: true)
    fileprivate let interactor = SlideAndDimInteractionController()
    
    @IBAction func show2ButtonPressed(_ sender: Any) {
        guard let VC2 = self.storyboard?.instantiateViewController(withIdentifier: "VC2ID") as? VC2 else {
                return
        }
        
        VC2.delegate = self
        VC2.transitioningDelegate = self
        VC2.modalPresentationStyle = .overCurrentContext
        
        self.animator.isHiding = false
        self.animator.interactive = false
        self.interactor.wireToViewController(VC2)
        self.present(VC2, animated: true, completion:nil)
    }
}

extension VC1: VC2Delegate {
    func didpressBackButton(_ viewController: VC2) {
        self.animator.isHiding = true
        self.animator.interactive = false
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension VC1: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("animationController(forPresented...): \(self.animator)")
        return self.animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.animator.isHiding = true
        print("animationController(forDismissed): \(self.animator)")
        return self.animator
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            let returnValue = self.animator.interactive ? self.interactor : nil
            print("interactionControllerForPresentation(using animator: \(animator)): \(String(describing: returnValue))")
            return returnValue
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            let returnValue = self.interactor.interactionInProgress ? self.interactor : nil
            print("interactionControllerForDismissal(using animator: \(animator)): \(String(describing: returnValue))")
            return returnValue
    }
}
