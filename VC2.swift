//
//  VC2.swift
//  JSinteractiveAnimatorTutorial
//
//  Created by Jerzyk on 24/05/2017.
//  Copyright © 2017 Jerzyk. All rights reserved.
//

import Foundation
import UIKit

protocol VC2Delegate: class {
    func didpressBackButton(_ viewController: VC2)
}
class VC2: UIViewController {
    weak var delegate: VC2Delegate?
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.delegate?.didpressBackButton(self)
    }
}

